﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSharp_Lab1.view;

namespace CSharp_Lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            new TerminalHandler().Start();
        }
    }
}
