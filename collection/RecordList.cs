﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Lab1.collection
{
    class RecordList
    {
        private List<Record> list;

        public RecordList()
        {
            list = new List<Record>();
        }
        public void Add(Record rec)
        {
            list.Add(rec);
        }

        public Record FindById(int id)
        {
            return list.Find(e => e.Id == id);
        }

        public bool Delete(int id)
        {
            return list.Remove(list.Find(e => e.Id == id));
        }

        public bool ChangeField(Record rec, Field field, string value)
        {
            value = value.Trim();
            if (value == "")
                return false;

            switch (field)
            {
                case Field.FIRST_NAME:
                    rec.FirstName = value;
                    break;
                case Field.LAST_NAME:
                    rec.LastName = value;
                    break;
                case Field.SECOND_NAME:
                    rec.SecondName = value;
                    break;
                case Field.PHONE_NUMBER:
                    long num;
                    if (!long.TryParse(value, out num))
                        return false;
                    rec.PhoneNumber = num;
                    break;
                case Field.COUNTRY:
                    rec.Country = value;
                    break;
                case Field.BORN_DATE:
                    DateTime dt;
                    if (!DateTime.TryParse(value, out dt))
                        return false;
                    rec.BornDate = dt;
                    break;
                case Field.ORGANIZATION:
                    rec.Organization = value;
                    break;
                case Field.POST:
                    rec.Post = value;
                    break;
                case Field.OTHER_NOTES:
                    rec.OtherNotes = value;
                    break;
            }
            return true;
        }

        public void PrintFullInfo(Record rec)
        {
            PrintFullInfo(list.IndexOf(rec));
        }
        public void PrintFullInfo(int index)
        {
            Console.WriteLine($"ID: {list[index].Id}");

            Console.WriteLine($"Фамилия: {list[index].LastName}");

            Console.WriteLine($"Имя: {list[index].FirstName}");

            if(list[index].SecondName == null || list[index].SecondName == "") Console.WriteLine("Отчество: Не указано");
            else Console.WriteLine($"Отчество: {list[index].SecondName}");

            Console.WriteLine($"Номер телефона: {list[index].PhoneNumber}");

            if (list[index].Country == null || list[index].Country == "")  Console.WriteLine("Страна: Не указано");
            else Console.WriteLine($"Страна: {list[index].Country}");

            if(list[index].BornDate == null || list[index].BornDate == DateTime.MinValue) Console.WriteLine("Дата рождения: Не указано");
            else Console.WriteLine($"Дата рождения: {list[index].BornDate.ToString("dd.MM.yyyy")}");

            if (list[index].Organization == null || list[index].Organization == "") Console.WriteLine($"Организация: Не указано");
            else Console.WriteLine($"Организация: {list[index].Organization}");

            if (list[index].Post == null || list[index].Post == "") Console.WriteLine($"Должность: Не указано");
            else Console.WriteLine($"Должность: {list[index].Post}");

            if (list[index].OtherNotes == null || list[index].OtherNotes == "") Console.WriteLine($"Прочие заметки: Не указано");
            else Console.WriteLine($"Прочие заметки: {list[index].OtherNotes}");

            Console.WriteLine();
        }

        public bool PrintAll()
        {
            if (list.Count == 0)
            {
                Console.WriteLine("Пусто");
                return false;
            }
            else
                foreach (Record rec in list)
                {
                    Console.WriteLine($"ID: {rec.Id}");
                    Console.WriteLine($"Фамилия: {rec.LastName}");
                    Console.WriteLine($"Имя: {rec.FirstName}");
                    Console.WriteLine($"Номер телефона: {rec.PhoneNumber}");
                    Console.WriteLine();
                }
            return true;
                
        }
    }

    enum Field
    {
        LAST_NAME,
        FIRST_NAME,
        SECOND_NAME,
        PHONE_NUMBER,
        COUNTRY,
        BORN_DATE,
        ORGANIZATION,
        POST,
        OTHER_NOTES
    }
}
