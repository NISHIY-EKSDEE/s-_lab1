﻿using CSharp_Lab1.collection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Lab1.view
{
    class TerminalHandler
    {

        RecordList rList;

        public TerminalHandler()
        {
            rList = new RecordList();
        }
        public void Start()
        {
            string cmd;
            Console.WriteLine("Программа запущена");
            Console.WriteLine("Здравствуй, пользователь!");
            Console.WriteLine("Введите \'help\' для справки");
            while (true)
            {
                cmd = Console.ReadLine();
                if(cmd == "help")
                {
                    Console.Clear();
                    Console.WriteLine("1) Создание новой записи");
                    Console.WriteLine("2) Редактирование записей");
                    Console.WriteLine("3) Удаление записей");
                    Console.WriteLine("4) Подробный просмотр одной из записей");
                    Console.WriteLine("5) Просмотр всех записей");
                }
                else
                {
                    int code;
                    if(!int.TryParse(cmd, out code))
                    {
                        Console.WriteLine("Введите номер команды (\'help\' для просмотра команд)");
                        continue;
                    }
                    switch (code)
                    {
                        case 1:
                            CreateNewRecord();
                            break;
                        case 2:
                            EditRecords();
                            break;
                        case 3:
                            DeleteRecords();
                            break;
                        case 4:
                            LookFullInfo();
                            break;
                        case 5:
                            Console.Clear();
                            rList.PrintAll();
                            break;
                        default:
                            Console.WriteLine("Неизвестная команда (\'help\' для просмотра команд)");
                            break;
                    }

                }

            }
        }

        private void CreateNewRecord()
        {
            Console.Clear();
            string lastName = GetCorrectString("Введите фамилию (обязательное поле)", true);
            string firstName = GetCorrectString("Введите имя (обязательное поле)", true);
            string secondName = GetCorrectString("Введите отчество (необязательное поле - можно оставить пустым)", false);
            long number = GetCorrectLong("Введите номер телефона (обязательное поле)", true);
            string country = GetCorrectString("Введите страну (обязательное поле)", true);
            DateTime bornDate = GetCorrectDateTime("Введите дату рождения (необязательное поле - можно оставить пустым)", false);
            string organization = GetCorrectString("Введите организацию (необязательное поле - можно оставить пустым)", false);
            string post = GetCorrectString("Введите должность (необязательное поле - можно оставить пустым)", false);
            string notes = GetCorrectString("Введите прочие заметки (необязательное поле - можно оставить пустым)", false);
            Record record = new Record(lastName, firstName, number, country);
            record.SecondName = secondName;
            record.BornDate = bornDate;
            record.Organization = organization;
            record.Post = post;
            record.OtherNotes = notes;
            rList.Add(record);
            Console.WriteLine("Готово");
            Console.Clear();
        }

        private void EditRecords()
        {
            Console.Clear();
            if (!rList.PrintAll())
            {
                Console.WriteLine("Нечего менять");
                return;
            }
            Console.WriteLine("Введите Id записи, которую нужно редактировать");
            int id, cmd;
            Record rec;
            while (true)
            {
                if (!int.TryParse(Console.ReadLine(), out id)) Console.WriteLine("Введите корректное значение");
                else
                {
                    rec = rList.FindById(id);
                    if (rec == null) Console.WriteLine("Записи с таким Id не существует. Попробуйте снова");
                    else break;
                }
            }
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Выберите команду");
                Console.WriteLine("1) Изменить фамилию");
                Console.WriteLine("2) Изменить имя");
                Console.WriteLine("3) Изменить отчество");
                Console.WriteLine("4) Изменить номер телефона");
                Console.WriteLine("5) Изменить страну");
                Console.WriteLine("6) Изменить дату рождения");
                Console.WriteLine("7) Изменить организацию");
                Console.WriteLine("8) Изменить должность");
                Console.WriteLine("9) Изменить прочие заметки");
                Console.WriteLine("10) Вывести информацию о текущей записи");
                Console.WriteLine("11) Выйти из меню редактирования");
                while (true)
                {
                    if (!int.TryParse(Console.ReadLine(), out cmd)) Console.WriteLine("Введите корректное значение");
                    else break;
                }
                switch (cmd)
                {
                    case 1:
                        Console.WriteLine("Введите новое значение");
                        if(!rList.ChangeField(rec, Field.LAST_NAME, Console.ReadLine()))
                        Console.WriteLine("Некорректное значение");
                        break;
                    case 2:
                        Console.WriteLine("Введите новое значение");
                        if (!rList.ChangeField(rec, Field.FIRST_NAME, Console.ReadLine()))
                        Console.WriteLine("Некорректное значение");
                        break;
                    case 3:
                        Console.WriteLine("Введите новое значение");
                        if (!rList.ChangeField(rec, Field.SECOND_NAME, Console.ReadLine()))
                        Console.WriteLine("Некорректное значение");
                        break;
                    case 4:
                        Console.WriteLine("Введите новое значение");
                        if (!rList.ChangeField(rec, Field.PHONE_NUMBER, Console.ReadLine()))
                        Console.WriteLine("Некорректное значение");
                        break;
                    case 5:
                        Console.WriteLine("Введите новое значение");
                        if (!rList.ChangeField(rec, Field.COUNTRY, Console.ReadLine()))
                        Console.WriteLine("Некорректное значение");
                        break;
                    case 6:
                        Console.WriteLine("Введите новое значение");
                        if (!rList.ChangeField(rec, Field.BORN_DATE, Console.ReadLine()))
                        Console.WriteLine("Некорректное значение");
                        break;
                    case 7:
                        Console.WriteLine("Введите новое значение");
                        if (!rList.ChangeField(rec, Field.ORGANIZATION, Console.ReadLine()))
                        Console.WriteLine("Некорректное значение");
                        break;
                    case 8:
                        Console.WriteLine("Введите новое значение");
                        if (!rList.ChangeField(rec, Field.POST, Console.ReadLine()))
                        Console.WriteLine("Некорректное значение");
                        break;
                    case 9:
                        Console.WriteLine("Введите новое значение");
                        if (!rList.ChangeField(rec, Field.OTHER_NOTES, Console.ReadLine()))
                        Console.WriteLine("Некорректное значение");
                        break;
                    case 10:
                        rList.PrintFullInfo(rec);
                        Console.WriteLine("Нажмите любую клавишу для продолжения редактирования");
                        Console.ReadKey();
                        break;
                    case 11:
                        return;
                    default:
                        Console.WriteLine("Введите корректную команду");
                        break;
                }
            }
        }

        private void DeleteRecords()
        {
            Console.Clear();
            if (!rList.PrintAll())
            {
                Console.WriteLine("Нечего удалять");
                return;
            }
            Console.WriteLine("Введите Id записи, которую нужно удалить");
            int id;
            Record rec;
            while (true)
            {
                if (!int.TryParse(Console.ReadLine(), out id)) Console.WriteLine("Введите корректное значение");
                else
                {
                    rec = rList.FindById(id);
                    if (rec == null) Console.WriteLine("Записи с таким Id не существует. Попробуйте снова");
                    else break;
                }
            }
            if (rList.Delete(id)) Console.WriteLine("Успешно удалено");
            else Console.WriteLine("Произошла ошибка");
        }

        private void LookFullInfo()
        {
            Console.Clear();
            if (!rList.PrintAll())
            {
                Console.WriteLine("Нечего смотреть");
                return;
            }
            Console.WriteLine("Введите Id записи, которую нужно просмотреть");
            int id;
            Record rec;
            while (true)
            {
                if (!int.TryParse(Console.ReadLine(), out id)) Console.WriteLine("Введите корректное значение");
                else
                {
                    rec = rList.FindById(id);
                    if (rec == null) Console.WriteLine("Записи с таким Id не существует. Попробуйте снова");
                    else break;
                }
            }
            rList.PrintFullInfo(rec);
        }
        private string GetCorrectString(string info, bool necessarily)
        {
            string s;
            Console.WriteLine(info);
            while (true)
            {
                s = Console.ReadLine().Trim();
                if ((s == null || s == "") && necessarily) Console.WriteLine("Введите корректное значение");
                else break;
            }
            return s;
        }

        private long GetCorrectLong(string info, bool necessarily)
        {
            long n;
            Console.WriteLine(info);
            while (true)
            {
                if (!long.TryParse(Console.ReadLine(), out n)) Console.WriteLine("Введите корректное значение");
                else break;
            }
            return n;
        }

        private DateTime GeeetCorrectDateTime(string info, bool necessarily)
        {
            DateTime dt;
            string cmd;
            Console.WriteLine(info);
            while (true)
            {
                cmd = Console.ReadLine();
                if ((cmd == null || cmd == "") && !necessarily) return DateTime.MinValue;
                else if (!DateTime.TryParse(cmd, out dt)) Console.WriteLine("Введите корректное значение");
                else break;
            }
            return dt;
        }
    }
}
