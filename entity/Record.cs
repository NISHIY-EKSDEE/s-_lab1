﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Lab1
{
    class Record
    {
        private static long recordsCount = 0;
        public long Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public long PhoneNumber { get; set; }
        public string Country { get; set; }
        public DateTime BornDate { get; set; }
        public string Organization { get; set; }
        public string Post { get; set; }
        public string OtherNotes { get; set; }

        public Record(string lastName, string firstName, long number, string country)
        {
            Id = ++recordsCount;
            LastName = lastName;
            FirstName = firstName;
            PhoneNumber = number;
            Country = country;
        }
    }
}
